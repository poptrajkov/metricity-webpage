import { Component, OnInit, Renderer2 } from '@angular/core';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.css']
})
export class AboutComponent implements OnInit {

  constructor(private renderer: Renderer2) { 
    // this.renderer.setStyle(document.body, 'background', '#FBBE7D');
    this.renderer.setStyle(document.body, 'background' , 'url(assets/img/front7.jpg) no-repeat center center fixed');
    this.renderer.setStyle(document.body, 'background-size','cover');
    this.renderer.setStyle(document.body, 'height','100%;');
  }

  ngOnInit() {
  }

}
