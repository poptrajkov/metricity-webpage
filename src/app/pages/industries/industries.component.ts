import { Component, OnInit, Renderer2 } from '@angular/core';

@Component({
  selector: 'app-industries',
  templateUrl: './industries.component.html',
  styleUrls: ['./industries.component.css']
})
export class IndustriesComponent implements OnInit {

  constructor(private renderer: Renderer2) { 
    // this.renderer.setStyle(document.body, 'background', '#6899AA');
    this.renderer.setStyle(document.body, 'background' , 'url(assets/img/front7.jpg) no-repeat center center fixed');
    this.renderer.setStyle(document.body, 'background-size','cover');
    this.renderer.setStyle(document.body, 'height','100%;');
  }

  ngOnInit() {
  }

}
