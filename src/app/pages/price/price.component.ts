import { Component, OnInit, Renderer2 } from '@angular/core';

@Component({
  selector: 'app-price',
  templateUrl: './price.component.html',
  styleUrls: ['./price.component.css']
})
export class PriceComponent implements OnInit {

  constructor(private renderer: Renderer2) { 
    this.renderer.setStyle(document.body, 'background' , 'url(assets/img/front7.jpg) no-repeat center center fixed');
    this.renderer.setStyle(document.body, 'background-size','cover');
    this.renderer.setStyle(document.body, 'height','100%;');
    this.renderer.setStyle(document.body, 'overflow-y','scroll');
    this.renderer.setStyle(document.body, 'overflow-x','hidden');
  }

  ngOnInit() {
  }

}
