import { Component, OnInit, Renderer2 } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(private renderer: Renderer2) { 
    this.renderer.setStyle(document.body, 'background' , 'url(assets/img/front7.jpg) no-repeat center center fixed');
    this.renderer.setStyle(document.body, 'background-size','cover');
    this.renderer.setStyle(document.body, 'height','100%;');
    // this.renderer.setStyle(document.body, 'overflow','scroll');
  }

  ngOnInit() {
  }

}
